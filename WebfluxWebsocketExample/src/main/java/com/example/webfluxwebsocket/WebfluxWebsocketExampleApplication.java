package com.example.webfluxwebsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxWebsocketExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxWebsocketExampleApplication.class, args);
    }

}
