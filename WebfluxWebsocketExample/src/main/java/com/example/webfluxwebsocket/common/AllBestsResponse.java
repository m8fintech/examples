package com.example.webfluxwebsocket.common;

import lombok.Value;

@Value
public class AllBestsResponse {
    String msg;
}
