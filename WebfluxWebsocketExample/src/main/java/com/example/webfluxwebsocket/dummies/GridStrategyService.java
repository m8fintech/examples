package com.example.webfluxwebsocket.dummies;

import com.example.webfluxwebsocket.common.GetStrategiesWithDataResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class GridStrategyService {
    public Flux<GetStrategiesWithDataResponse> getStrategiesWithData() {
        return Flux.just(new GetStrategiesWithDataResponse("This is test GetStrategiesWithDataResponse message"));
    }
}
