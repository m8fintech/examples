package com.example.webfluxwebsocket.dummies;

import com.example.webfluxwebsocket.common.AllBestsResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class ExchangeService {

    public Flux<AllBestsResponse> getAllBests(){
        return Flux.just(new AllBestsResponse("This is test AllBestsResponse message"));
    }
}
