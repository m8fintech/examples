package com.example.webfluxwebsocket.controller;

import com.example.webfluxwebsocket.common.AllBestsResponse;
import com.example.webfluxwebsocket.common.GetStrategiesWithDataResponse;
import com.example.webfluxwebsocket.dummies.ExchangeService;
import com.example.webfluxwebsocket.dummies.GridStrategyService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.Loggers;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Component
public class WebsocketController {
    private static final Duration SNAPSHOT_UPDATES_INTERVAL = Duration.ofSeconds(10);

    @Bean
    public HandlerMapping webSocketHandlerMapping(WebSocketHandler snapshotsWebsocketHandler) {
        Map<String, WebSocketHandler> map = new HashMap<>();
        map.put("/snapshots", snapshotsWebsocketHandler);

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setOrder(1);
        handlerMapping.setUrlMap(map);
        return handlerMapping;
    }

    @Bean
    public WebSocketHandler snapshotsWebsocketHandler(MessageSerializer mapper,
                                                      Flux<AllBestsResponse> priceSnapshotsFlux,
                                                      Flux<GetStrategiesWithDataResponse> strategySnapshotsFlux) {

        return session -> Mono.defer(() -> {
            val inputLogger = Loggers.getLogger("ws_in_" + session.getId());
            val outputLogger = Loggers.getLogger("ws_out_" + session.getId());

            val input = session.receive()
                    .log(inputLogger)
                    .then();

            val outputPublisher = Flux.merge(priceSnapshotsFlux, strategySnapshotsFlux)
                    .map(it -> session.textMessage(mapper.writeValueAsString(it)))
                    .log(outputLogger);

            val output = session.send(outputPublisher)
                    .then();

            return Mono.zip(input, output).then();
        });
    }

    @Bean
    Flux<AllBestsResponse> priceSnapshotsFlux(ExchangeService service) {
        return Flux.interval(Duration.ZERO, SNAPSHOT_UPDATES_INTERVAL)
                .flatMap(unused -> service.getAllBests())
                .share();
    }

    @Bean
    Flux<GetStrategiesWithDataResponse> strategySnapshotsFlux(GridStrategyService service) {
        return Flux.interval(Duration.ZERO, SNAPSHOT_UPDATES_INTERVAL)
                .flatMap(unused -> service.getStrategiesWithData())
                .share();
    }

    @Bean
    public MessageSerializer messageSerializer(ObjectMapper mapper) {
        return object -> {
            try {
                return mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        };
    }

    public interface MessageSerializer {
        String writeValueAsString(Object object);
    }
}